# UtilityLK

This is a collection of some random C libraries I have created over time.

## License

All libraries are dual licensed.
You can either used them under the 3-clause BSD license,
or treat them as public domain code.
Some libraries are based on ones made by other people, though. You obviously still need to properly credit them!
See LICENSE for details.

## What's in here

This is a collection of some random C libraries I have created over time.

## License

All libraries are licensed under the 3-clause BSD license. See LICENSE.md for details.

## How to use

Just grab the header file and the corresponding c source file and add them to your project. You may need to update the include path in the src (.c) file.

## What's in here

|library|version|category|description|
|---|---|---|---|
|[ULK_vector](include/ULK_vector.h)|1.1|math|simple vector math library with support for 2,3 and 4 dimensional vectors|
|[ULK_matrix](include/ULK_matrix.h)|1.1.1|math|simple matrix math library with support for 2x2,2x3,3x3 and 4x4 matrix, needs ULK_vector to work|
